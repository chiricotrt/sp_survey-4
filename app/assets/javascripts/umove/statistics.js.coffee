## MAIN CALL
jQuery ->
  $("#start_date_picker").datepicker
    "dateFormat": "dd-mm-yy"
    "altField": "#start_date"
    "altFormat": "yy-mm-dd"

  $("#end_date_picker").datepicker
    "dateFormat": "dd-mm-yy"
    "altField": "#end_date"
    "altFormat": "yy-mm-dd"

  ## Retrieve Statistics ##
  $("#summarise").click ->
    key = $(this).attr("name")
    value = $(this).val()

    if key == "test_maas_app"
      if value == "1"
        $("#feedback_maas_app_section").show()
      else
        $("#feedback_maas_app_section").hide()

    request = $.get("summarise", { id: $("#city").val(), start_date: $("#start_date").val(), end_date: $("#end_date").val() })
    request.success (data) ->
      $("#residents_registered").html(data.residents.registered)
      $("#residents_completed").html(data.residents.completed)
      $("#residents_age_groups").html(JSON.stringify(data.residents.age_groups, null, 2))
      $("#residents_gender").html(JSON.stringify(data.residents.genders, null, 2))
      $("#residents_driving_licences").html(JSON.stringify(data.residents.driving_licences, null, 2))
      $("#residents_car_sharing").html(JSON.stringify(data.residents.car_sharing_groups, null, 2))
      $("#residents_education_levels").html(JSON.stringify(data.residents.education_levels, null, 2))

      $("#tourists_registered").html(data.tourists.registered)
      $("#tourists_completed").html(data.tourists.completed)

      return false