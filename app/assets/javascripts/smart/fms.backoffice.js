var smart;
if (smart === undefined) {
  smart = {};
}

smart.Backoffice = {};

smart.Backoffice.handle_user_selection = function(){
  var user_id = jQuery('#people').val();
  console.log("user_id_selected:"+user_id)

  jQuery('.run_detect_stops').unbind('click');
  jQuery('.run_consume_raw_data').unbind('click');
  jQuery('.send_push_notification').unbind('click');

  jQuery.ajax({
    url: Routes.backoffice_getPersonInfo_path(),
    data: {id: user_id},
    type: "POST",
    dataType: "json",
    success: function(data){
      if (!data.hasOwnProperty('user')) {
        jQuery('.userinfo').html('invalid user_id provided: ' + user_id);
        return;
      }

      jQuery('.userinfo').html(jQuery('#template_user_info').jqote(data));
      jQuery('.user_options').html(jQuery('#template_user_options').jqote(data));

      jQuery('.run_detect_stops').click(function(e){
        var user_id = jQuery(e.target).attr('rel');
        smart.Backoffice.show_postJobDialog(user_id, 'detect_stops', 'Run Detect Stops');
      })

      jQuery('.run_consume_raw_data').click(function(e){
        var user_id = jQuery(e.target).attr('rel');
        smart.Backoffice.show_postJobDialog(user_id, 'consume_raw_data', 'Run Consume Raw Data');
      })

      jQuery('.send_push_notification').click(function(e){
        var user_id = jQuery(e.target).attr('rel');
        smart.Backoffice.show_pushNotificationDialog(user_id);
      })
    }
  });
}

smart.Backoffice.show_pushNotificationDialog = function(user_id){
  var container_name = ".push_notification_dialog";
  var user_id_elm = container_name + ' input[name="user_id"]';
  var title_elm = container_name + ' input[name="title"]';
  var body_elm = container_name + ' input[name="body"]';

  var ds_dialog = jQuery(container_name)
  if (ds_dialog.length == 0){
    jQuery('body').append(jQuery('#template_push_notification').jqote())
  }

  $(".push_notification_dialog").modal();

  jQuery(user_id_elm).val(user_id);

  jQuery(container_name+' button[name="submit"]').unbind('click');
  jQuery(container_name+' button[name="submit"]').bind('click',function(){
    var title = $(title_elm).val();
    var body = $(body_elm).val();

    jQuery.ajax({
      url: Routes.send_push_path(user_id),
      data: {title: title, body: body},
      type: "POST",
      dataType: "json",
      success: function(data){
        smart.Alert.Success('Successfully sent a push notification for user '+ user_id, false);
      },
      error: function(){
        smart.Alert.Error('Failed to send a push notification for user '+ user_id, true);
      }
    });

    $(".push_notification_dialog").modal('hide');
  });

}

smart.Backoffice.show_postJobDialog = function(user_id, job_name, title){
  var container_name = '.user_job_dialog';
  var start_date_elm = container_name+'  input[name="start_date"]';
  var start_time_elm = container_name+'  input[name="start_time"]';
  var end_date_elm = container_name+'  input[name="end_date"]';
  var end_time_elm = container_name+'  input[name="end_time"]';
  var user_id_elm = container_name+' input[name="user_id"]';


  var ds_dialog = jQuery(container_name)
  if (ds_dialog.length == 0){
    jQuery('body').append(jQuery('#template_run_user_jobs').jqote())
  }

  $(".user_job_dialog").modal();

  jQuery(user_id_elm).val(user_id);

  var datepicker_options = {
    dateFormat: "yy-mm-dd"
  }

  if (title!= null){
    jQuery(container_name + ' .modal-title').html(title);
  }
  jQuery(start_date_elm).datepicker(datepicker_options);
  jQuery(start_time_elm).timepicker();
  jQuery(end_date_elm).datepicker(datepicker_options);
  jQuery(end_time_elm).timepicker();

  jQuery(container_name+' button[name="submit"]').unbind('click');
  jQuery(container_name+' button[name="submit"]').bind('click',function(){
    var start_date = jQuery(start_date_elm).datepicker('getDate');
    var start_time = jQuery(start_time_elm).timepicker('getTime');
    var end_date = jQuery(end_date_elm).datepicker('getDate');
    var end_time = jQuery(end_time_elm).timepicker('getTime');
    var user_id = jQuery(user_id_elm).val();

    var date_format = "YYYY-MM-DD";

    var new_date_start = moment(start_date).format(date_format) +" "+ start_time;
    var new_date_end = moment(end_date).format(date_format) +" "+ end_time;

    var postObj = {
      user_id: user_id,
      start_time: new_date_start,
      end_time: new_date_end,
      job_name: job_name
    }

    jQuery.ajax({
      url: Routes.backoffice_postJob_path(),
      data: postObj,
      type: "POST",
      dataType: "json",
      success: function(data){
        smart.Alert.Success('Successfully posted a '+job_name+' job for user '+ user_id, false);
      },
      error: function(){
        smart.Alert.Error('Failed to post a '+job_name+' job for user '+ user_id, true);
      }
    });

    $(".user_job_dialog").modal('hide');
  });
}

smart.Backoffice.unload_dsDialog = function(parent_elm){
  console.log("unloading datepickers")
  jQuery(parent_elm+' input[name="start_date"]').datepicker('destroy');
  jQuery(parent_elm+' input[name="start_time"]').timepicker('destroy');
  jQuery(parent_elm+' input[name="end_date"]').datepicker('destroy');
  jQuery(parent_elm+' input[name="end_time"]').timepicker('destroy');
}
