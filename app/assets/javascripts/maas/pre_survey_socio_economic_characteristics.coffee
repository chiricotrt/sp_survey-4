jQuery ->
  setDependencies()
  setAnswers()

setDependencies = ->
  # On change of employment_status
  # $("#pre_survey_socio_economic_characteristic_employment_status").change ->
  #   if $("#pre_survey_socio_economic_characteristic_employment_status").val() and $("#pre_survey_socio_economic_characteristic_employment_status").val() < 6
  #     # $("#work-related").show()
  #   else
  #     # $("#work-related").hide()
  #     $("#pre_survey_socio_economic_characteristic_employment_flexibility").val("")

  # On change of disability_status
  $("#pre_survey_socio_economic_characteristic_disability_status").change ->
    console.log $("#pre_survey_socio_economic_characteristic_disability_status").val()
    if $("#pre_survey_socio_economic_characteristic_disability_status").val() == "1"
      $("#disability-related").show()
    else
      # TODO Reset mode values
      $("#disability-related").hide()

setAnswers = ->
  # if employment_status < 7, show work-related questions
  # if $("#pre_survey_socio_economic_characteristic_employment_status").val() and $("#pre_survey_socio_economic_characteristic_employment_status").val() < 6
  #   $("#work-related").show()

  # disability-status
  if $("#pre_survey_socio_economic_characteristic_disability_status").val() and $("#pre_survey_socio_economic_characteristic_disability_status").val() == "1"
    $("#disability-related").show()