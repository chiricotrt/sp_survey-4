jQuery ->
  $(".has-tip").tooltip()

  ## SETTING QUESTIONS ##
  # if count-qn changes and is > 1, show other-members qn
  $("#count_qn").change ->
    if $("#count_qn").val() > 1
      $("#number_of_children").show()
    else
      $("#number_of_children").hide()

  # ------------------------------------------------------------------------------------------ #

  ## SETTING ANSWERS ##
  # Toggle dependent questions (depends on answers)
  $(".pre_survey_household_characteristic_other_members").hide()
  if $("#count_qn").val() > 1
    $("#number_of_children").show()
  else
    $("#number_of_children").hide()
  # ------------------------------------------------------------------------------------------ #
