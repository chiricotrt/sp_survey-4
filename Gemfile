source 'https://rubygems.org'

ruby '2.6.7'
gem 'rails', '~> 4.2.6'

# Bundle edge Rails instead:
# gem 'rails', github: 'rails/rails'

gem 'protected_attributes'
gem 'activerecord-session_store'

# gem 'sqlite3'

# Use SCSS for stylesheets
gem 'sass-rails',   '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

gem 'bootstrap-sass', '~> 3.3.4'
gem "non-stupid-digest-assets" # needed for static error pages

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'momentjs-rails'
gem 'js-routes'
gem 'wiselinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0',          group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Unicorn as the app server
# gem 'unicorn'
# gem 'unicorn-worker-killer'

# Deploy with Capistrano
# gem 'capistrano', :github => 'capistrano/capistrano', :branch => 'legacy-v2'
gem 'exception_notification'

# bundler requires these gems in all environments
# gem "nokogiri", "1.4.2"
# gem "geokit"

gem "json", "~>1.7"
gem "mysql2", '0.4.10'
gem 'authlogic', '3.5.0'
gem 'scrypt'
gem "whenever",  "~>0.8"

gem "delayed_job" #, :path=> 'vendor/plugins/delayed_job'
gem 'delayed_job_active_record'
gem "delayed_job_web"
gem 'daemons'
gem "acl9" #, :path => 'vendor/plugins/acl9'
#gem "scout_rails"
#gem "scout"
# gem "jammit", "~>0.6"
gem "yui-compressor", "~>0.11"
# gem "capistrano-maintenance"
gem "oj", "~>2.0"
gem "grocer"
gem 'rpush', github: 'rpush/rpush'
gem "gcm"
gem "sentry-raven", '~> 1.1.0' #, :github => "getsentry/raven-ruby",  :branch => 'rails2'
gem "faraday", "~>0.8"
gem "faraday_middleware"
# gem 'surveyor', github: 'prusswan/surveyor', branch: 'heh-master-rails41' # fork of official repo with Rails 4.1 + jquery validation support
gem 'surveyor', git: 'https://github.com/sridharraman/surveyor.git', branch: 'heh-master-rails41'
# AFTER gem 'surveyor', github: 'fmsurvey/surveyor', branch: 'heh-master-rails41' # fork of official repo with Rails 4.1 + jquery validation support
gem 'validates_overlap'
gem 'timezone', "0.3.2"

# mongo
# nosql db storage for external-api data
gem 'mongoid', '~> 5.1.0'

group :development do
  # bundler requires these gems in development
  # gem "rails-footnotes"
  gem "ruby-prof"
  gem 'aescrypt'
  gem 'activesupport'
  gem 'quiet_assets'
  gem 'annotate', '~> 2.6.5'
  gem 'pry-rails'
  gem 'thin', '~> 1.8'
  gem "bullet"
  gem "letter_opener"
  gem 'bootstrap-generators', '~> 3.3.4'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 3.0'

  gem 'rails_real_favicon'
end

group :test do
  # bundler requires these gems while running tests
  # gem "rspec-rails", "~> 1.3"
  # gem "faker"
  gem 'simplecov', :require => false
end

group :test, :development do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  gem "minitest-rails" #, "~> 1.0"
  gem 'minitest-rails-capybara'
  gem 'minitest-reporters'
  gem 'database_cleaner'
  gem 'mocha'

  gem 'selenium-webdriver'

  gem 'guard'
  gem 'guard-minitest'

  gem 'pry'
  gem 'awesome_print'

  gem 'teaspoon', github: 'modeset/teaspoon'
  gem 'teaspoon-qunit'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
end
gem 'rubyzip', "=1.1.7"
gem 'zip-zip' # will load compatibility for old rubyzip API.
gem 'redis'
gem 'geocoder', '~> 1.6.7'

gem "knnball", "= 0.0.5"
gem "polylines"
#gem "gsl", "= 1.14.7"

gem 'newrelic_rpm'
gem "figaro"

gem "iron_fixture_extractor", github: 'umn-asr/iron_fixture_extractor', branch: 'rails_4' # < Rails 4.2 due to serialize_attributes deprecation

gem 'rack-cors', :require => 'rack/cors'
gem 'active_model_serializers', github: 'rails-api/active_model_serializers', branch: '0-9-stable'

gem 'ruby_parser', :require => false, :group => :development

# form building
gem 'simple_form', "3.3.1" #"3.2.1" # earlier 3.3.1
gem 'bootstrap_form', "2.5.0" #"2.3.0" # earlier 2.5.0

# mysql spatial
# gem 'activerecord-mysql2spatial-adapter' #, '~> 0.5.0.nonrelease'

# consuming api
gem 'httparty'

# string matching
gem 'fuzzy_match'

# time formatting
gem 'chronic_duration'

# page-click tracking
gem 'impressionist'

group :production do
  gem 'rails_12factor'
end

# Capistrano Deployment #sridhar
group :development do
  gem 'capistrano',         require: false
  # gem 'capistrano-rvm',     require: false
  gem 'capistrano-rbenv',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
  gem 'capistrano-database-yml', '~> 1.0.0'
  gem 'sshkit-sudo'
  gem 'erb2haml'
end

gem 'puma'

# Browser detection
gem "browser", require: "browser/browser"

# Provides list of countries
gem 'country_select'

# IP detection
gem 'geoip2_compat'

# JSON traversing
gem 'jsonpath'

gem 'sprockets', '~>3.7.2'
gem 'haml'
gem 'haml-rails'

# Use postgresql as the database for Active Record
gem 'pg', '~> 0.15'

gem 'ordinalize_full', '~> 1.6.0'
gem 'humanize', '~> 2.5.1'