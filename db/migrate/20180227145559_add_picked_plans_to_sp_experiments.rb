class AddPickedPlansToSpExperiments < ActiveRecord::Migration
  def change
    add_column :sp_experiments, :picked_plans, :text
  end
end
