class AddTsLastUploadToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :ts_last_upload, :timestamp
  end

  def self.down
    remove_column :users, :ts_last_upload
  end
end
