class AddBrowserToUsers < ActiveRecord::Migration
  def change
    add_column :users, :browser, :string
  end
end
